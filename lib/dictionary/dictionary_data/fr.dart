import 'package:Provider_template_v1/dictionary/dictionary_classes/about_us_page_language.dart';
import 'package:Provider_template_v1/dictionary/dictionary_classes/main_page_language.dart';
import 'package:Provider_template_v1/dictionary/models/language.dart';

const fr = Language(
  aboutUsPageLanguage: AboutUsPageLanguage(
    companyDescriptionText: 'De',
    writeUsTitle:
        // TODO(Alex): Split this to more short strings, like in EN/IT
        // TODO(Ihor): done
        'Nous avoir parole la nous moussant. Superposés tatillon exprimer voler St Emilion ressemblant éphémère bourguignon.'
        'Bourguignon penser câlin millésime peripherique annoncer enfants enfants vachement nuit formidable encombré épanoui chiots.'
        ' Arc truc cacatoès lorem flâner',
    emailFieldText: 'Écrivez-nous',
    messageFieldText: 'Message',
    sendButtonText: 'Envoyer',
    nameFieldText: 'Nom',
    appBarTitle: 'À propos de nous',
    // TODO(Alex): add coma
    // TODO(Ihor): done
  ),

);
