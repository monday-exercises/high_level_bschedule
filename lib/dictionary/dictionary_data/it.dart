import 'package:Provider_template_v1/dictionary/dictionary_classes/about_us_page_language.dart';
import 'package:Provider_template_v1/dictionary/dictionary_classes/create_record_page_language.dart';
import 'package:Provider_template_v1/dictionary/dictionary_classes/history_page_language.dart';
import 'package:Provider_template_v1/dictionary/dictionary_classes/languages_page_language.dart';
import 'package:Provider_template_v1/dictionary/dictionary_classes/main_page_language.dart';
import 'package:Provider_template_v1/dictionary/dictionary_classes/onboarding_page_language.dart';
import 'package:Provider_template_v1/dictionary/dictionary_classes/pop_up_helper_language.dart';
import 'package:Provider_template_v1/dictionary/dictionary_classes/settings_page_language.dart';
import 'package:Provider_template_v1/dictionary/models/language.dart';

const it = Language(
  aboutUsPageLanguage: AboutUsPageLanguage(
      companyDescriptionText: null,
      writeUsTitle:
          'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna '
          'aliqua. Duis at tellus at urna condimentum mattis pellentesque id. Nulla facilisi nullam vehicula ipsum a. '
          'Tincidunt eget nullam non nisi est sit. Sit amet massa vitae tortor condimentum lacinia. Sed vulputate odio '
          'ut enim blandit volutpat maecenas volutpat blandit. Sem et tortor consequat id porta nibh venenatis cras. '
          'Nisi est sit amet facilisis magna etiam.',
      emailFieldText: 'E-mail',
      messageFieldText: 'Messaggio',
      sendButtonText: 'Spedire',
      nameFieldText: 'Nome',
      // TODO(Alex): add come
      appBarTitle: 'Riguardo a noi'),
  mainPageLanguage: MainPageLanguage(firstNote: "asdasd"),
  popUpHelperLanguage: PopUpHelperLanguage(cancel: "asd", save: 'asdasd'),
  onBoardingPageLanguage:
      OnBoardingPageLanguage(createRecord: 'asdas', manageRecord: 'asdasd', fillRecord: 'asdasd', miss: 'asdasdas'),
  createRecordPageLanguage: CreateRecordPageLanguage(
      name: 'asdasd',
      dateCome: 'asdasd',
      price: 'asdasd',
      timeBegin: 'asdasd',
      timeEnd: 'asdasd',
      addServiceHint: 'asdasd',
      description: 'asdasd',
      color: 'asdasd',
      add: 'asdasd',
      delete: 'asdasd',
      create: 'asdasd',
      appBarTitle: 'asdasd',
      service: 'asdasd',
      fillFields: 'asdasd'),
  languagesPageLanguage:
      LanguagesPageLanguage(appBarTitle: 'asdasd', english: 'asdasd', france: 'asdasd', italia: 'asdasd'),
  historyPageLanguage: HistoryPageLanguage(kAppBarTitle: 'asdasd', kSearchText: 'asdasd', kCancel: 'asdasd'),
  settingsPageLanguage: SettingsPageLanguage(language: 'asdasd', aboutUs: 'asdasd', kAppBarTitle: 'asdasd')
);
