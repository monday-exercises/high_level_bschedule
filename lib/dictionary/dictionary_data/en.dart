import 'package:Provider_template_v1/dictionary/dictionary_classes/about_us_page_language.dart';
import 'package:Provider_template_v1/dictionary/dictionary_classes/create_record_page_language.dart';
import 'package:Provider_template_v1/dictionary/dictionary_classes/history_page_language.dart';
import 'package:Provider_template_v1/dictionary/dictionary_classes/languages_page_language.dart';
import 'package:Provider_template_v1/dictionary/dictionary_classes/main_page_language.dart';
import 'package:Provider_template_v1/dictionary/dictionary_classes/onboarding_page_language.dart';
import 'package:Provider_template_v1/dictionary/dictionary_classes/pop_up_helper_language.dart';
import 'package:Provider_template_v1/dictionary/dictionary_classes/settings_page_language.dart';
import 'package:Provider_template_v1/dictionary/models/language.dart';



const Language en = Language(
  aboutUsPageLanguage: AboutUsPageLanguage(
      companyDescriptionText:
          'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna '
          'aliqua. Duis at tellus at urna condimentum mattis pellentesque id. Nulla facilisi nullam vehicula ipsum a. '
          'Tincidunt eget nullam non nisi est sit. Sit amet massa vitae tortor condimentum lacinia. Sed vulputate odio '
          'ut enim blandit volutpat maecenas volutpat blandit. Sem et tortor consequat id porta nibh venenatis cras. '
          'Nisi est sit amet facilisis magna etiam.',
      writeUsTitle: 'Write us',
      emailFieldText: 'Email',
      messageFieldText: 'Message',
      sendButtonText: 'Send',
      nameFieldText: 'Name',
      appBarTitle: 'About Us',
      emailError: 'Enter correct email!',
      fillFields: 'Fill all fields!',
      textError: 'Enter some text!'),
  // TODO(Alex): add coma
  // TODO(Ihor): done
  settingsPageLanguage: SettingsPageLanguage(
    language: 'Language',
    aboutUs: 'About us',
    kAppBarTitle: 'Settings',
  ),
  historyPageLanguage: HistoryPageLanguage(
    kAppBarTitle: 'History',
    kSearchText: 'Search client or service',
    kCancel: 'Cancel',
  ),
  languagesPageLanguage: LanguagesPageLanguage(
    appBarTitle: 'Language',
    english: 'English',
    france: 'France',
    italia: 'Italia',
  ),
  createRecordPageLanguage: CreateRecordPageLanguage(
    name: 'Name',
    color: 'Color',
    price: 'Price',
    create: 'Create',
    add: 'Add',
    addServiceHint: 'Add new service',
    appBarTitle: 'Create Record',
    dateCome: 'Date come',
    delete: 'Delete',
    description: 'Desciption',
    timeBegin: 'Time begin',
    timeEnd: 'Time end',
    service: 'Service',
    fillFields: 'Fill all fields!',
  ),
  onBoardingPageLanguage: OnBoardingPageLanguage(
      createRecord: 'Tap for button to create a record',
      manageRecord: 'Here you can see your clients, delete, edit and view all information when clicking on a client',
      fillRecord: 'Fill in your customer data, choose the date, price, service',
      miss: 'Miss'),
  popUpHelperLanguage: PopUpHelperLanguage(
    save: 'Save',
    cancel: 'Cancel',
  ),
  mainPageLanguage: MainPageLanguage(firstNote: 'Create your first note!'),
);
