import 'package:Provider_template_v1/dictionary/dictionary_classes/about_us_page_language.dart';
import 'package:Provider_template_v1/dictionary/dictionary_classes/create_record_page_language.dart';
import 'package:Provider_template_v1/dictionary/dictionary_classes/history_page_language.dart';
import 'package:Provider_template_v1/dictionary/dictionary_classes/languages_page_language.dart';
import 'package:Provider_template_v1/dictionary/dictionary_classes/main_page_language.dart';
import 'package:Provider_template_v1/dictionary/dictionary_classes/onboarding_page_language.dart';
import 'package:Provider_template_v1/dictionary/dictionary_classes/pop_up_helper_language.dart';
import 'package:Provider_template_v1/dictionary/dictionary_classes/settings_page_language.dart';

//import 'package:exercises/day_19/practise_8_localization/dictionary/dictionary_classes/general_language.dart';
import 'package:flutter/cupertino.dart';

class Language {
//  final GeneralLanguage generalLanguage;
  final AboutUsPageLanguage aboutUsPageLanguage;
  final SettingsPageLanguage settingsPageLanguage;
  final HistoryPageLanguage historyPageLanguage;
  final LanguagesPageLanguage languagesPageLanguage;
  final CreateRecordPageLanguage createRecordPageLanguage;
  final OnBoardingPageLanguage onBoardingPageLanguage;
  final PopUpHelperLanguage popUpHelperLanguage;
  final MainPageLanguage mainPageLanguage;

  const Language({
//    @required this.generalLanguage,
    @required this.aboutUsPageLanguage,
    @required this.settingsPageLanguage,
    @required this.historyPageLanguage,
    @required this.languagesPageLanguage,
    @required this.createRecordPageLanguage,
    @required this.onBoardingPageLanguage,
    @required this.popUpHelperLanguage,
    @required this.mainPageLanguage,
  });
}
