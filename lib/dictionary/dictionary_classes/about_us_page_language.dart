import 'package:flutter/cupertino.dart';

class AboutUsPageLanguage {
  final String companyDescriptionText;
  final String writeUsTitle;
  final String emailFieldText;
  final String messageFieldText;
  final String sendButtonText;
  final String nameFieldText;
  final String appBarTitle;
  final String emailError;
  final String textError;
  final String fillFields;

  const AboutUsPageLanguage({
    @required this.companyDescriptionText,
    @required this.writeUsTitle,
    @required this.emailFieldText,
    @required this.messageFieldText,
    @required this.sendButtonText,
    @required this.nameFieldText,
    @required this.appBarTitle,
    @required this.emailError,
    @required this.textError,
    @required this.fillFields,
  });
}
