import 'package:flutter/material.dart';

class PopUpHelperLanguage {
  final String cancel;
  final String save;

  const PopUpHelperLanguage({
    @required this.cancel,
    @required this.save,
  });
}
