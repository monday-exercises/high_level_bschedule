import 'package:flutter/material.dart';

class MainPageLanguage {
  final String firstNote;

  const MainPageLanguage({@required this.firstNote});
}
