import 'package:flutter/cupertino.dart';

class CreateRecordPageLanguage {
  final String name;
  final String dateCome;
  final String price;
  final String timeBegin;
  final String timeEnd;
  final String service;
  final String addServiceHint;
  final String description;
  final String color;
  final String add;
  final String delete;
  final String create;
  final String appBarTitle;
  final String fillFields;

  const CreateRecordPageLanguage({
    @required this.name,
    @required this.dateCome,
    @required this.price,
    @required this.timeBegin,
    @required this.timeEnd,
    @required this.addServiceHint,
    @required this.description,
    @required this.color,
    @required this.add,
    @required this.delete,
    @required this.create,
    @required this.appBarTitle,
    @required this.service,
    @required this.fillFields,
  });
}
