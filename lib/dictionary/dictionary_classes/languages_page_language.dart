import 'package:flutter/material.dart';

class LanguagesPageLanguage {
  final String appBarTitle;
  final String english;
  final String france;
  final String italia;

  const LanguagesPageLanguage({
    @required this.appBarTitle,
    @required this.english,
    @required this.france,
    @required this.italia,
  });
}
