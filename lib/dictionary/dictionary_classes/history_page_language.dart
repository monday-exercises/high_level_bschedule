import 'package:flutter/material.dart';

class HistoryPageLanguage {
  final String kAppBarTitle;
  final String kSearchText;
  final String kCancel;

  const HistoryPageLanguage({
    @required this.kAppBarTitle,
    @required this.kSearchText,
    @required this.kCancel,
  });
}
