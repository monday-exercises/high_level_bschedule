import 'package:flutter/material.dart';

class OnBoardingPageLanguage {
  final String createRecord;
  final String manageRecord;
  final String fillRecord;
  final String miss;

  const OnBoardingPageLanguage({
    @required this.createRecord,
    @required this.manageRecord,
    @required this.fillRecord,
    @required this.miss,
  });
}
