import 'package:flutter/material.dart';

class SettingsPageLanguage {
  final String language;
  final  String aboutUs;
  final String kAppBarTitle;

  const SettingsPageLanguage({@required this.language, @required this.aboutUs, @required this.kAppBarTitle});
}
