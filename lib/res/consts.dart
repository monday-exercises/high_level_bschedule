// region [Language]
const String kLocaleEn = 'en';
const String kLocaleFr = 'fr';
const String kLocaleIt = 'it';

// endregion

// region [Global]
const String kEmptyString = '';
// endregion

// TODO(Alex): remove unused consts, if you specify them in dictionary
// TODO(Alex): remove comments
// TODO(Ihor): done
const double kDesignScreenHeight = 812;
const double kDesignScreenWidth = 375;
const bool kDesignAllowFontScaling = true;
