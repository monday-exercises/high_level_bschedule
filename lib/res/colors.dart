import 'package:flutter/material.dart';

class AppColors {
  static const Color kWhite = Color(0xFFFFFFFF);
  static const Color kBottomNavigationColor = Color(0xFF272727);
  static const Color kGrey = Color(0xFF6A6A6A);
  static const Color kBigBlueButtonColor = Color(0xFF33B5FF);
  static const Color kSplashScreenBackgroundColor = Color(0xFF009EF8);
  static const Color kBottomNavigationButtonColor = Color(0xFF32A6E8);
  static const Color kBlack = Color(0xFF000000);
  static const Color kDodgerBlue = Color(0xFF33B5FF);
  static const Color kBrightSkyBlue = Color(0xFF00C6FF);
  static const Color kBrightBlue = Color(0xFF0072FF);
  static const Color kSlate = Color(0xFF536976);
  static const Color kDark = Color(0xFF292e49);
  static const Color kHotPink = Color(0xFFec008c);
  static const Color kSalmon = Color(0xFFfc6767);
  static const Color kNeonPurple = Color(0xFFda22ff);
  static const Color kBluishPurple = Color(0xFF9733ee);
  static const Color kTealish = Color(0xFF24c6dc);
  static const Color kBlueberry = Color(0xFF514a9d);
  static const Color kBlack80 = Color(0xFF272727);
  static const Color kBlack0 = Color(0xFF000000);
  static const Color kBlackTwo = Color(0xFF373737);
  static const Color kWhiteTwo = Color(0xFFf2f2f2);


}
