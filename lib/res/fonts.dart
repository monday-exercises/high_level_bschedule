import 'package:Provider_template_v1/res/consts.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

// TODO(Alex): don't use ./ or ../ imports.
// TODO(Alex): don't use ./ or ../ imports.
/// use full imports like
/// import 'package:Provider_template_v1/res/colors.dart';

import './colors.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class FontStyles {
  TextStyle example() {
    return TextStyle();
  }

  static TextStyle get loaderText {
    return GoogleFonts.montserrat(
      color: AppColors.kWhiteTwo,
      fontSize: 14.0.sp,
      fontWeight: FontWeight.bold,
    );
  }

  static TextStyle get kMontserratBold12  {
    return GoogleFonts.montserrat(
      color: Colors.black,
      fontSize: 12.sp,
      fontWeight: FontWeight.bold,
    );
  }

  static TextStyle get kMontserratBoldWhite32  {
    return GoogleFonts.montserrat(
      color: AppColors.kWhiteTwo,
      fontSize: 32.sp,
      fontWeight: FontWeight.bold,
    );
  }

  static TextStyle get kMontserratBoldWhite18  {
    return GoogleFonts.montserrat(
      color: AppColors.kWhiteTwo,
      fontSize: 18.sp,
      fontWeight: FontWeight.bold,
    );
  }

  static TextStyle get kMontserratBold24 {
    return GoogleFonts.montserrat(
      color: AppColors.kBlack,
      fontSize: 24.sp,
      fontWeight: FontWeight.bold,
    );
  }
  static TextStyle get kMontserratBoldWhite22  {
    return GoogleFonts.montserrat(
      color: AppColors.kWhiteTwo,
      fontSize: 23.sp,
      fontWeight: FontWeight.bold,
    );
  }

  static TextStyle get kMontserratWhiteBold18  {
    return GoogleFonts.montserrat(
      color: AppColors.kWhiteTwo,
      fontSize: 18.sp,
      fontWeight: FontWeight.bold,
    );
  }

  static TextStyle get kMontserratMediumGrey12 {
    return GoogleFonts.montserrat(
      color: AppColors.kGrey,
      fontSize: 12.sp,
      fontWeight: FontWeight.w500,
    );
  }
  static TextStyle get kMontserratMediumWhite12 {
    return GoogleFonts.montserrat(
      color: AppColors.kWhiteTwo,
      fontSize: 12.sp,
      fontWeight: FontWeight.w500,
    );
  }

  static TextStyle get kMontserratMediumBlue12 {
    return GoogleFonts.montserrat(
      color: AppColors.kDodgerBlue,
      fontSize: 12.sp,
      fontWeight: FontWeight.w500,
    );
  }

  static TextStyle get kMontserratSemiBoldWhite14 {
    return GoogleFonts.montserrat(
      color: AppColors.kWhiteTwo,
      fontSize: 14.sp,
      fontWeight: FontWeight.w600,
    );
  }
  static TextStyle get kMontserratSemiBoldGrey14 {
    return GoogleFonts.montserrat(
      color: AppColors.kGrey,
      fontSize: 14.sp,
      fontWeight: FontWeight.w600,
    );
  }




  static TextStyle get kSubHeader {
    return GoogleFonts.montserrat(
      color: AppColors.kBlack,
      fontSize: 18.sp,
      fontWeight: FontWeight.bold,
    );
  }

  static TextStyle get kEditText {
    return GoogleFonts.montserrat(
      color: AppColors.kBlack,
      fontSize: 14.sp,
      fontWeight: FontWeight.w600,
    );
  }

  static TextStyle get kMontserrat {
    return GoogleFonts.montserrat(
      color: AppColors.kBlack,
      fontSize: 17.sp,
//      fontWeight: FontWeight.w600,
    );
  }

}
