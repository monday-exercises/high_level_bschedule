class Routes {
  static const kHistoryPage = '/History';
  static const kMainPage = '/Main';
  static const kSettingsPage = '/Settings';
  static const kAboutUsPage = '/About-us';
  static const kSplashPage = '/';
  static const kLanguagesPage = '/Languages';
  static const kCreateRecordPage = '/Create';
}
