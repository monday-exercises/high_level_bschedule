import 'package:Provider_template_v1/data/models/record.dart';
import 'package:Provider_template_v1/dictionary/flutter_delegate.dart';
import 'package:Provider_template_v1/dictionary/models/supported_locales.dart';
import 'package:Provider_template_v1/helpers/route_helper.dart';
import 'package:Provider_template_v1/providers/bottom_navigation_provider.dart';
import 'package:Provider_template_v1/providers/calendar_provider.dart';
import 'package:Provider_template_v1/providers/create_record_page_provider.dart';
import 'package:Provider_template_v1/providers/language_provider.dart';
import 'package:Provider_template_v1/providers/on_boarding_pointer_provider.dart';
import 'package:Provider_template_v1/providers/record_provider.dart';
import 'package:Provider_template_v1/res/consts.dart';
import 'package:Provider_template_v1/ui/layouts/clean_behavior.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

// TODO(Alex): remove local imports; remove unused imports
// TODO(Ihor): done
class Application extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle(
          statusBarColor: Colors.transparent,
          statusBarIconBrightness: Brightness.dark,
          systemNavigationBarColor: Colors.black,
          systemNavigationBarDividerColor: Colors.black),
      child: MultiProvider(
        providers: [
          ChangeNotifierProvider.value(
            value: BottomNavigationProvider(),
          ),
          ChangeNotifierProvider.value(
            value: LanguageProvider(),
          ),
          ChangeNotifierProvider.value(
            value: OnBoardingPointerProvider(),
          ),
          ChangeNotifierProvider.value(
            value: RecordProvider(),
          ),
          ChangeNotifierProvider.value(
            value: CreateRecordPageProvider(),
          ),
          ChangeNotifierProvider.value(
            value: CalendarProvider(),
          ),
          ChangeNotifierProxyProvider<CalendarProvider, RecordProvider>(
            create: (ctx) {
              return RecordProvider(selectedDate: DateTime.now(), records: <Record>[]);
            },
            update: (ctx, selectedDate, cards) {
              return RecordProvider(selectedDate: selectedDate.selectedDate, records: cards.records);
            },
          ),
        ],
        child: MaterialApp(
          builder: (context, child) {
            return ScrollConfiguration(
              behavior: CleanBehavior(),
              child: child,
            );
          },
          title: kEmptyString,
          locale: Locale(FlutterDictionaryDelegate.getCurrentLocale),
          supportedLocales: SupportedLocales.instance.getSupportedLocales,
          localizationsDelegates: FlutterDictionaryDelegate.getLocalizationDelegates,
          debugShowCheckedModeBanner: false,
          onGenerateRoute: RouteHelper.onGenerateRoute,
        ),
      ),
    );
  }
}
