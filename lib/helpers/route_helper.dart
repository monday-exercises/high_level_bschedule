import 'package:Provider_template_v1/res/routes.dart';
import 'package:Provider_template_v1/ui/pages/create_record_page/create_record.dart';
import 'package:Provider_template_v1/ui/pages/history_page/history_page.dart';
import 'package:Provider_template_v1/ui/pages/main_page/main_page.dart';
import 'package:Provider_template_v1/ui/pages/settings_page/settings_page.dart';
import 'file:///C:/Users/AppVesto/Projects/igor_bscheduled/lib/ui/pages/about_us_page/about_us_page.dart';
import 'file:///C:/Users/AppVesto/Projects/igor_bscheduled/lib/ui/pages/languages_page/languages_page.dart';
import 'package:Provider_template_v1/ui/pages/support/splash_screen.dart';
import 'package:Provider_template_v1/ui/pages/support/unknown_page.dart';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';

class RouteHelper {
  static const String TAG = '[Service]';
  static List<String> routeStack = [Routes.kMainPage];

  RouteHelper._privateConstructor();

  static final RouteHelper _instance = RouteHelper._privateConstructor();

  static RouteHelper get instance => _instance;

  static Route<dynamic> onGenerateRoute(RouteSettings settings) {
//    if (!routeStack.contains(settings.name)) {
//      routeStack.
//    }
//    print(routeStack.length);

    switch (settings.name) {
    // TODO(Alex): add coma
    // TODO(Ihor): done
      case Routes.kSplashPage:
        {
          return PageTransition(
            child: SplashScreen(),
            type: PageTransitionType.fade,
            settings: settings,
          );
        }
      case Routes.kMainPage:
        {
          return PageTransition(
            child: MainPage(),
            type: PageTransitionType.fade,
            settings: settings,
          );
        }
      case Routes.kHistoryPage:
        {
          return PageTransition(
            child: HistoryPage(),
            type: PageTransitionType.fade,
            settings: settings,
          );
        }

      case Routes.kSettingsPage:
        {
          return PageTransition(
            child: SettingsPage(),
            type: PageTransitionType.fade,
            settings: settings,
          );
        }
      case Routes.kAboutUsPage:
        {
          return PageTransition(
            child: AboutUsPage(),
            type: PageTransitionType.fade,
            settings: settings,
          );
        }
      case Routes.kLanguagesPage:
        {
          return PageTransition(
            child: LanguagesPage(),
            type: PageTransitionType.fade,
            settings: settings,
          );
        }
      case Routes.kCreateRecordPage:
        {
          return PageTransition(
            child: CreateRecordPage(),
            type: PageTransitionType.fade,
            settings: settings,
          );
        }

      default:
        return _defaultRoute(
          settings: settings,
          page: UnknownPage(),
        );
    }
  }

  static MaterialPageRoute _defaultRoute({RouteSettings settings, Widget page}) {
    return MaterialPageRoute(
      settings: settings,
      builder: (BuildContext context) => page,
    );
  }

  String getPreviousPageName() {
    if (routeStack.length >= 2) {
//      print('LENGTH>2');
      return routeStack[routeStack.length - 2];
    } else {
      return Routes.kMainPage;
    }
  }

  void push(BuildContext ctx, String routeName) {
    routeStack.add(routeName);
//    print(routeStack);

    Navigator.of(ctx).pushNamed(routeName);
  }

  void pushReplacement(BuildContext ctx, String routeName) {
//    if(routeStack.length <= 1){
//      routeStack.add(routeName);
//    }

    routeStack.removeLast();

    routeStack.add(routeName);
    Navigator.of(ctx).pushReplacementNamed(routeName);
  }

}

//void pop(BuildContext ctx) {
//  if (routeStack.length > 1) {
//    routeStack.removeLast();
//  } else {
//    throw Exception('You dont have pages in route stack!');
//  }
//
//  Navigator.of(ctx).pop();
//}}
