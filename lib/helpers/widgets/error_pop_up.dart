import 'package:Provider_template_v1/dictionary/dictionary_classes/pop_up_helper_language.dart';
import 'package:Provider_template_v1/dictionary/dictionary_data/en.dart';
import 'package:Provider_template_v1/dictionary/flutter_dictionary.dart';
import 'package:Provider_template_v1/res/colors.dart';
import 'package:Provider_template_v1/res/fonts.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class ErrorPopUp extends StatelessWidget {
  final String errorText;

  ErrorPopUp({@required this.errorText});

  @override
  Widget build(BuildContext context) {
    PopUpHelperLanguage language = FlutterDictionary.instance.language?.popUpHelperLanguage ?? en.popUpHelperLanguage;
    return Container(
      height: 200.0.h,
      padding: const EdgeInsets.all(8.0),
      color: AppColors.kWhite,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Center(
            child: Text(
              errorText,
              style: TextStyle(fontSize: 25.sp),
            ),
          ),
          SizedBox(
            height: 20.0.h,
          ),
          InkWell(
            onTap: () {
              Navigator.of(context).pop();
            },
            child: Center(
              child: Text(
                language.cancel,
                style: FontStyles.kMontserratMediumGrey12,
              ),
            ),
          )
        ],
      ),
    );
  }
}
