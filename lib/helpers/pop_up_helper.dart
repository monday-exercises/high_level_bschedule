import 'package:Provider_template_v1/dictionary/dictionary_classes/create_record_page_language.dart';
import 'package:Provider_template_v1/dictionary/dictionary_classes/onboarding_page_language.dart';
import 'package:Provider_template_v1/dictionary/dictionary_data/en.dart';
import 'package:Provider_template_v1/dictionary/flutter_dictionary.dart';
import 'package:Provider_template_v1/helpers/widgets/error_pop_up.dart';
import 'package:Provider_template_v1/providers/on_boarding_pointer_provider.dart';
import 'package:Provider_template_v1/res/colors.dart';
import 'package:Provider_template_v1/res/fonts.dart';
import 'package:Provider_template_v1/ui/layouts/dialog_layout.dart';
import 'package:Provider_template_v1/ui/pages/create_record_page/widgets/date_picker.dart';
import 'package:Provider_template_v1/ui/pages/create_record_page/widgets/price_picker.dart';
import 'package:Provider_template_v1/ui/pages/create_record_page/widgets/time_picker.dart';
import 'package:Provider_template_v1/ui/shared/on_boarding.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:provider/provider.dart';

class PopUpHelper {
  // region [Initialize]
  static const String TAG = '[Service]';

  PopUpHelper._privateConstructor();

  static final PopUpHelper _instance = PopUpHelper._privateConstructor();

  static PopUpHelper get instance => _instance;

  // endregion


  void showErrorPopUp({BuildContext ctx, String errorText}) {
    showDialog(
      context: ctx,
      builder: (ctx) => DialogLayout(
        child: ErrorPopUp(errorText: errorText,),
      ),
    );
  }

  void showTimePicker({BuildContext context, DateTime initialDateTime, void Function(DateTime) saveDate}) {
    showDialog(
      context: context,
      builder: (context) => DialogLayout(
        child: TimePicker(
          initialTime: initialDateTime,
          saveDate: saveDate,
        ),
      ),
    );
  }

  void showOnBoardingPopUp(BuildContext context) {
    showDialog(
      context: context,
      builder: (context) => DialogLayout(
        child: OnBoarding(),
      ),
    );
  }

  void showPricePicker({BuildContext context, double initialPrice, void Function(double) newPrice}) {
    showDialog(
      context: context,
      builder: (context) => DialogLayout(
        child: PricePicker(
          initialPrice: initialPrice,
          savePrice: newPrice,
        ),
      ),
    );
  }

  void showDatePicker({BuildContext context, DateTime initialDate, void Function(DateTime) saveDate}) {
    showDialog(
      context: context,
      builder: (context) => DialogLayout(
          child: DatePicker(
        initialDate: initialDate,
        saveDate: saveDate,
      )),
    );
  }
}
