import 'package:flutter/material.dart';

class Record {
  String name;
  DateTime dateCome;
  DateTime creationDate = DateTime.now();
  DateTime timeBegin;
  DateTime timeEnd;
  double price;
  String category;
  String description;
  List<Color> colors;

  Record({
    @required this.name,
    @required this.dateCome,
    @required this.timeBegin,
    @required this.timeEnd,
    @required this.price,
    @required this.category,
    @required this.description,
    @required this.colors,
  });

  @override
  String toString() {
    return 'Record{name: $name, dateCome: $dateCome, creationDate: $creationDate, timeBegin: $timeBegin, timeEnd: $timeEnd, price: $price, category: $category, description: $description, colors: $colors}';
  }
}
