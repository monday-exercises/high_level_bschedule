import 'package:Provider_template_v1/data/models/record.dart';
import 'package:Provider_template_v1/res/colors.dart';
import 'package:Provider_template_v1/res/fonts.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:intl/intl.dart';

class RecordCard extends StatelessWidget {
  Record record;

  RecordCard({@required this.record});

  @override
  Widget build(BuildContext context) {
    print(record);
    return Card(
      elevation: 5,
      shape: BeveledRectangleBorder(
        borderRadius: BorderRadius.circular(15.0),
      ),
      child: Container(
        height: 190.0.h,
        width: 335.0.w,
        padding: EdgeInsets.all(14.0.w),
        decoration: BoxDecoration(
          gradient: record.colors.isEmpty
              ? LinearGradient(colors: [AppColors.kDodgerBlue, AppColors.kDodgerBlue])
              : LinearGradient(colors: [record.colors[0], record.colors[1]]),
          borderRadius: BorderRadius.circular(15.0),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  record.name,
                  style: FontStyles.kMontserratBoldWhite18,
                ),
                Row(
                  children: [
                    Icon(
                      CupertinoIcons.trash,
                      color: AppColors.kWhiteTwo,
                    ),
                    SizedBox(
                      width: 26.0.w,
                    ),
                    Icon(
                      CupertinoIcons.pen,
                      color: AppColors.kWhiteTwo,
                    ),
                  ],
                ),
              ],
            ),
            Spacer(flex: 3),
            Text(record.category, style: FontStyles.kMontserratSemiBoldWhite14),
            Spacer(),
            Text(
              record.description,
              style: FontStyles.kMontserratSemiBoldWhite14,
            ),
            Divider(height: 1.h),
            Spacer(flex: 3),
            Row(
              children: [
                Text(
                  '${DateFormat('d.M.y').format(record.dateCome)}',
                  style: FontStyles.kMontserratSemiBoldWhite14,
                ),
                Spacer(),
                Text(
                  '${DateFormat('Hm').format(record.timeBegin)}',
                  style: FontStyles.kMontserratSemiBoldWhite14,
                ),
                if (record.timeEnd != null)
                  Text(
                    ' - ${DateFormat('Hm').format(record.timeEnd)}',
                    style: FontStyles.kMontserratSemiBoldWhite14,
                  ),
                Spacer(
                  flex: 5,
                ),
                Text(
                  '${record.price}',
                  style: FontStyles.kMontserratSemiBoldWhite14,
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
