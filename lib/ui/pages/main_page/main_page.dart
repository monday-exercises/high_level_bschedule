import 'package:Provider_template_v1/dictionary/dictionary_classes/main_page_language.dart';
import 'package:Provider_template_v1/dictionary/dictionary_data/en.dart';
import 'package:Provider_template_v1/dictionary/flutter_dictionary.dart';
import 'package:Provider_template_v1/providers/record_provider.dart';
import 'package:Provider_template_v1/res/fonts.dart';
import 'package:Provider_template_v1/ui/layouts/main_layout.dart';
import 'package:Provider_template_v1/ui/pages/main_page/widgets/record_card.dart';
import 'package:Provider_template_v1/ui/shared/custom_app_bar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:provider/provider.dart';

//var initScreen;

// ignore: use_key_in_widget_constructors
class MainPage extends StatefulWidget {
  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final MainPageLanguage language = FlutterDictionary.instance.language?.mainPageLanguage ?? en.mainPageLanguage;

    // TODO(Alex): use autoformatting
    // TODO(Ihor): done

    return MainLayout(
      isFloatingActionBtn: true,
      appBar: CustomAppBar(),
      child: Consumer<RecordProvider>(
        builder: (ctx, recordProviderData, ch) {
          return recordProviderData.getRecordsByDate().isEmpty || recordProviderData.getRecordsByDate()[0] == null
              ? ch
              : SizedBox(
                  height: 800.h,
                  child: Column(
                    children: recordProviderData.getRecordsByDate().map((record) {
                      return RecordCard(record: record);
                    }).toList(),
                  ),
                );
        },
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            Text(language.firstNote, style: FontStyles.kMontserratBold24),
            Image.asset(
              'assets/images/first_note.png',
              height: 449.0.h,
              width: 241.0.w,
            ),
            SizedBox(width: 100.0.h),
            // TODO(Alex): double
            // TODO(Ihor): done
          ],
        ),
      ),
    );
  }
}
