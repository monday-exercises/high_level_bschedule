import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class UnknownPage extends StatelessWidget {
  // TODO(Alex): remove unused functions from template
  // TODO(Alex): remove unused functions from template
  /// also, make this page stateless
  //TODO(Ihor): done
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
        alignment: Alignment.center,
        child: Text(
          'Unknown Page',
          style: GoogleFonts.montserrat(
            color: Colors.black,
            fontSize: 24,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
    );
  }
}
