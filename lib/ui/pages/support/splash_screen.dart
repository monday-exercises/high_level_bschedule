import 'dart:async';

import 'package:Provider_template_v1/helpers/route_helper.dart';
import 'package:Provider_template_v1/res/colors.dart';
import 'package:Provider_template_v1/res/consts.dart';
import 'package:Provider_template_v1/res/routes.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:google_fonts/google_fonts.dart';

class SplashScreen extends StatefulWidget {
  static const routeName = '/';

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  // TODO(Alex): do all initialization after super.initState();
  // TODO(Ihor): done
  @override
  void initState() {
    super.initState();

    WidgetsBinding.instance.addPostFrameCallback((_) async {
      ScreenUtil.init(
        context,
        // TODO(Alex): use autoformatting
        // TODO(Ihor): done

        height: kDesignScreenHeight, width: kDesignScreenWidth, allowFontScaling: kDesignAllowFontScaling,
      );
    });

    _loadWidget();
  }

  Timer _loadWidget() {
    return Timer(
      Duration(seconds: 3),
      () =>
//          Navigator.of(context).pushReplacementNamed(Routes.kMainPage),
          RouteHelper.instance.pushReplacement(context, Routes.kMainPage),
    );
  }

  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;

    return Material(
      child: Container(
        color: AppColors.kSplashScreenBackgroundColor,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(
              height: deviceSize.height * 0.26,
              width: deviceSize.width * 0.6,
              // TODO(Alex): move this to res/assets/images.dart
              /// images.dart
              /// final String kCalendarImage = 'assets/images/calendar.png';
              child: Image.asset('assets/images/calendar.png'),
            ),
            SizedBox(height: deviceSize.height * 0.1),
            CupertinoActivityIndicator(),
            SizedBox(
              height: 20,
            ),
            Text(
              // TODO(Alex): move this to dictionary
              ///It may look pointless right now, but later you'd store preferred locale locally
              ///and this part will not be localised properly
              'loading',
              // TODO(Alex): move this to res/fonts
              style: GoogleFonts.montserrat(
                color: Colors.black,
                fontSize: 14,
                fontWeight: FontWeight.w600,
              ),
            )
          ],
        ),
      ),
    );
  }
}
