import 'package:Provider_template_v1/dictionary/dictionary_classes/about_us_page_language.dart';
import 'package:Provider_template_v1/dictionary/dictionary_data/en.dart';
import 'package:Provider_template_v1/dictionary/flutter_dictionary.dart';
import 'package:Provider_template_v1/helpers/pop_up_helper.dart';
import 'package:Provider_template_v1/res/colors.dart';
import 'package:Provider_template_v1/res/fonts.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class AboutUsForm extends StatefulWidget {
  @override
  _AboutUsFormState createState() => _AboutUsFormState();
}

class _AboutUsFormState extends State<AboutUsForm> {
  final _formKey = GlobalKey<FormState>();
  final _emailFocusNode = FocusNode();
  final _messageFocusNode = FocusNode();

  @override
  void dispose() {
    _emailFocusNode.dispose();
    _messageFocusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
//    final provider = Provider.of<LanguageProvider>(context);
    AboutUsPageLanguage language = FlutterDictionary.instance.language?.aboutUsPageLanguage ?? en.aboutUsPageLanguage;
    return Form(
      key: _formKey,
      child: Column(
        children: [
          TextFormField(
            validator: (value) {
              if (value.isEmpty) {
                // TODO(Alex): move this to dictionary
                return language.textError;
              }
              // TODO(Alex): what is this?
              // TODO(Ihor): value for validator
              return null;
            },
            decoration: InputDecoration(
              labelText: language.nameFieldText,
              labelStyle: TextStyle(color: Colors.black),
              // TODO(Alex): what is this?
              //TODO(Ihor): done
            ),
            onFieldSubmitted: (_) {
              FocusScope.of(context).requestFocus(_emailFocusNode);
            },
            textInputAction: TextInputAction.next,
          ),
          TextFormField(
            validator: (value) {
              if (value.isEmpty) {
                // TODO(Alex): move this to dictionary
                //TODO(Ihor): done
                return language.textError;
              }
              return null;
            },
            decoration: InputDecoration(labelText: language.emailFieldText),
            focusNode: _emailFocusNode,
            onFieldSubmitted: (_) {
              FocusScope.of(context).requestFocus(_messageFocusNode);
            },
          ),
          SizedBox(
            height: 10.0.h,
          ),
          TextFormField(
            validator: (value) {
              if (value.isEmpty) {
                // TODO(Alex): move this to dictionary
                return language.textError;
              } else if (!value.endsWith('.com') && !value.contains('@')) {
                PopUpHelper.instance.showErrorPopUp(
                  ctx: context,
                  errorText: language.emailError,
                );
                return language.emailError;
              }
              return null;
            },

            decoration: InputDecoration(
              labelText: language.messageFieldText,
              labelStyle: TextStyle(color: AppColors.kGrey),
              contentPadding: EdgeInsets.only(left: 5.0, top: 14.0),
              alignLabelWithHint: true,
              enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(width: 0.5),
              ),
            ),
            maxLines: 6,
//            keyboardType: TextInputType.multiline,
            focusNode: _messageFocusNode,
            textInputAction: TextInputAction.done,
          ),
          SizedBox(height: 32.0.h),
          SizedBox(
            width: double.infinity,
            height: 48.0.h,
            child: RaisedButton(
              elevation: 5,
//              padding: EdgeInsets.symmetric(vertical: 40.h,horizontal: 1.w),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(5),
              ),

              color: AppColors.kBigBlueButtonColor,
              textColor: AppColors.kWhite,
              child: Text(
                language.sendButtonText,
                style: FontStyles.kMontserratWhiteBold18,
              ),
              onPressed: () {
                final isValid = _formKey.currentState.validate();
                if (!isValid) {
                  PopUpHelper.instance.showErrorPopUp(ctx: context, errorText: language.fillFields);
                }
//                if (isValid) {
//                  _formKey.currentState.save();
//                }
//                provider.setNewLane(Locale(kLocaleIt));
              },
            ),
          ),
        ],
      ),
    );
  }
}
