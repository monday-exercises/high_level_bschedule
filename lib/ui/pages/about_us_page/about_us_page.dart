import 'package:Provider_template_v1/dictionary/dictionary_classes/about_us_page_language.dart';
import 'package:Provider_template_v1/dictionary/dictionary_data/en.dart';
import 'package:Provider_template_v1/dictionary/flutter_dictionary.dart';
import 'package:Provider_template_v1/helpers/route_helper.dart';
import 'package:Provider_template_v1/res/fonts.dart';
import 'package:Provider_template_v1/ui/layouts/main_layout.dart';
import 'package:Provider_template_v1/ui/pages/about_us_page/widgets/about_us_form.dart';
import 'package:Provider_template_v1/ui/shared/main_app_bar.dart';

/// import 'package:Provider_template_v1/ui/shared/main_app_bar.dart';
import 'package:auto_size_text/auto_size_text.dart';

// TODO(Alex): remove cupertino import
// TODO(Ihor): done

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class AboutUsPage extends StatelessWidget {
  // TODO(Alex): remove unused variables
  // TODO(Ihor): done

  @override
  Widget build(BuildContext context) {
    AboutUsPageLanguage dictionary = FlutterDictionary.instance.language?.aboutUsPageLanguage ?? en.aboutUsPageLanguage;
    return MainLayout(
      child: SingleChildScrollView(
        child: Column(
          children: [
            SizedBox(height: 32.0.h),
            SizedBox(
              width: 193.0.w,
              height: 36.0.h,
              child: Image.asset(
                'assets/images/appvesto_logo.png',
              ),
            ),
            SizedBox(height: 15.0.h),
            SizedBox(
                // TODO(Alex): add coma
                // TODO(Ihor): done
                height: 180.0.h,
                child: AutoSizeText(dictionary.companyDescriptionText, style: FontStyles.kMontserrat)),
            SizedBox(
              height: 40.0.h,
            ),
            Text(
              dictionary.writeUsTitle,
              style: FontStyles.kSubHeader,
              textAlign: TextAlign.center,
            ),
            SizedBox(
              height: 10.0.h,
            ),
            AboutUsForm(),
            SizedBox(
              height: 28.0.h,
            ),
          ],
        ),
      ),
      appBar: MainAppBar(
        title: dictionary.appBarTitle,
        previousRoute: RouteHelper.instance.getPreviousPageName().substring(1),
      ),
    );
  }
}
