import 'package:flutter/material.dart';

import '../languages_page.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class LanguageRadio extends StatelessWidget {
  String languageTitle;
  LanguageEnum value;
  String imageAsset;
  LanguageEnum groupValue;
  Function handleValueChange;

  LanguageRadio({
    @required this.languageTitle,
    @required this.value,
    @required this.imageAsset,
    @required this.groupValue,
    @required this.handleValueChange,
  });

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Image.asset(
          imageAsset,
          height: 28.0.h,
          width: 20.0.w,
        ),
//        SizedBox(
//          width: 9.0.w,
//        ),
        SizedBox(width: 50, child: Text(languageTitle)),
//       const SizedBox(width: 15.0),
        Radio(
          value: value,
          groupValue: groupValue,
          onChanged: (language) {
            handleValueChange(value);
          },
        ),
      ],
    );
  }
}
