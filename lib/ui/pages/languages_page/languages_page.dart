import 'package:Provider_template_v1/dictionary/dictionary_classes/languages_page_language.dart';
import 'package:Provider_template_v1/dictionary/dictionary_classes/settings_page_language.dart';
import 'package:Provider_template_v1/dictionary/dictionary_data/en.dart';
import 'package:Provider_template_v1/dictionary/flutter_dictionary.dart';
import 'package:Provider_template_v1/helpers/route_helper.dart';
import 'package:Provider_template_v1/providers/language_provider.dart';
import 'package:Provider_template_v1/res/consts.dart';
import 'package:Provider_template_v1/ui/layouts/main_layout.dart';
import 'package:Provider_template_v1/ui/pages/languages_page/widgets/language_radio.dart';
import 'package:Provider_template_v1/ui/shared/main_app_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:provider/provider.dart';

enum LanguageEnum { en, fr, it }

class LanguagesPage extends StatefulWidget {
  @override
  _LanguagesPageState createState() => _LanguagesPageState();
}

class _LanguagesPageState extends State<LanguagesPage> {
  LanguagesPageLanguage language =
      FlutterDictionary.instance.language?.languagesPageLanguage ?? en.languagesPageLanguage;
  LanguageEnum _radioValue;

  @override
  Widget build(BuildContext context) {
    return MainLayout(
      child: Column(
        children: [
//          LanguageRadio(languageTitle: language.english, value: LanguageEnum.en, imageAsset: 'assets/images/language_icons/gb_icon.png'),
          languageRadio(
            language.english,
            LanguageEnum.en,
            'assets/images/language_icons/gb_icon.png',
          ),
          // TODO(Alex): add coma
          // TODO(Ihor): done
          languageRadio(
            language.france,
            LanguageEnum.fr,
            'assets/images/language_icons/fr_icon.png',
          ),
          // TODO(Alex): add coma
          // TODO(Ihor): done
          languageRadio(
            language.italia,
            LanguageEnum.it,
            'assets/images/language_icons/it_icon.png',
          ),
//          SizedBox(width: 224.0.w),
          // TODO(Alex): add coma
          // TODO(Ihor): done
        ],
      ),
      appBar: MainAppBar(
        title: language.appBarTitle,
        previousRoute: RouteHelper.instance.getPreviousPageName().substring(1),
      ),
    );
  }

  // TODO(Alex): Prefer using named parameters
  // TODO(Alex): Move this to separate widget, not widget function
  Widget languageRadio(String languageTitle, LanguageEnum value, String imageAsset) {
    return SizedBox(
      width: 158.0.h,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Image.asset(
            imageAsset,
            height: 28.0.h,
            width: 20.0.w,
          ),
//        SizedBox(
//          width: 9.0.w,
//        ),
          SizedBox(width: 50, child: Text(languageTitle)),
//       const SizedBox(width: 15.0),
          Radio(
            value: value,
            groupValue: _radioValue,
            onChanged: (language) {
              __handleRadioValueChange(value);
            },
          ),
        ],
      ),
    );
  }

  // TODO(Alex): ????????????
  void __handleRadioValueChange(LanguageEnum radioValue) {
    final provider = Provider.of<LanguageProvider>(context, listen: false);
    setState(() {
      _radioValue = radioValue;
    });
    switch (_radioValue) {
      case LanguageEnum.en:
//                provider.setNewLane(Locale(kLocaleEn));
        break;
      case LanguageEnum.fr:
//              provider.setNewLane(Locale(kLocaleFr));
        break;
      case LanguageEnum.it:
        provider.setNewLane(Locale(kLocaleIt));
        break;
    }
  }
}
