import 'package:Provider_template_v1/res/colors.dart';
import 'package:Provider_template_v1/res/fonts.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter/material.dart';

class CustomBlueBox extends StatelessWidget {
  String descriptionText;
  String centerText;

  CustomBlueBox({
    @required this.centerText,
    @required this.descriptionText,
  });

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              descriptionText,
              style: FontStyles.kMontserratMediumGrey12,
            ),
            SizedBox(height: 4.0.h),
            Container(
              height: 36.0.h,
              width: 108.0.w,
              decoration: BoxDecoration(
                color: AppColors.kBigBlueButtonColor,
                borderRadius: BorderRadius.circular(12),
              ),
              child: Center(
                child: Text(
                  centerText,
                  style: FontStyles.kMontserratSemiBoldWhite14,
                ),
              ),
            ),
          ],
        ),
      ],
    );
  }
}
