import 'package:Provider_template_v1/dictionary/dictionary_classes/create_record_page_language.dart';
import 'package:Provider_template_v1/dictionary/dictionary_classes/pop_up_helper_language.dart';
import 'package:Provider_template_v1/dictionary/dictionary_data/en.dart';
import 'package:Provider_template_v1/dictionary/flutter_dictionary.dart';
import 'package:Provider_template_v1/res/colors.dart';
import 'package:Provider_template_v1/res/fonts.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class TimePicker extends StatelessWidget {
  final DateTime initialTime;
  void Function(DateTime) saveDate;
  int _minutes;
  int _hours;
  DateTime _selectedTime;

  TimePicker({
    @required this.initialTime,
    @required this.saveDate,
  }) {
    _minutes = initialTime.minute;
    _hours = initialTime.hour;
    _selectedTime = initialTime;
  }

  @override
  Widget build(BuildContext context) {
    CreateRecordPageLanguage timePickerLanguage =
        FlutterDictionary.instance.language?.createRecordPageLanguage ?? en.createRecordPageLanguage;
    PopUpHelperLanguage language = FlutterDictionary.instance.language?.popUpHelperLanguage ?? en.popUpHelperLanguage;


    return SizedBox(
      height: 371.0.h,
      width: 385.0.w,
      child: Stack(children: [
        Container(height: 371.0.h, width: 385.0.w, color: AppColors.kBlackTwo),
        Container(
          height: 371.0.h,
          width: 385.0.w,
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: [
                AppColors.kBlack80,
                Colors.black12,
                AppColors.kBlack80,
              ],
            ),
          ),
        ),
        Container(
          height: 335.0.h,
          width: 335.0.w,
          padding: const EdgeInsets.all(8.0),
          color: Colors.transparent,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Row(
                children: [
                  Spacer(),
                  InkWell(
                    onTap: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      language.cancel,
                      style: FontStyles.kMontserratMediumGrey12,
                    ),
                  ),
                  Spacer(
                    flex: 2,
                  ),
                  Text(
                    timePickerLanguage.timeBegin,
                    style: FontStyles.kMontserratSemiBoldWhite14,
                  ),
                  Spacer(
                    flex: 2,
                  ),
                  InkWell(
                    onTap: () {
                      _selectedTime == initialTime
                          ? saveDate(_selectedTime)
                          : saveDate(_selectedTime = DateTime(
                              _selectedTime.year,
                              _selectedTime.month,
                              _selectedTime.day,
                              _selectedTime.hour,
                              _selectedTime.minute,
                            ));
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      language.save,
                      style: FontStyles.kMontserratMediumBlue12,
                    ),
                  ),
                  Spacer(),
                ],
              ),
              SizedBox(
                height: 275.0.h,
                width: 450.0.w,
                child: CupertinoTheme(
                  data: CupertinoThemeData(
                    brightness: Brightness.dark,
                    textTheme: CupertinoTextThemeData(dateTimePickerTextStyle: FontStyles.kMontserratBoldWhite32),
                  ),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Spacer(flex: 3),
                      SizedBox(
                        width: 50.0.w,
                        child: CupertinoPicker(
                          scrollController: FixedExtentScrollController(initialItem: initialTime.hour),
                          squeeze: 0.7,
                          itemExtent: 36,
                          diameterRatio: 2,
                          magnification: 1.1,
                          looping: true,
                          onSelectedItemChanged: (i) {
                            _hours = i;
                            updateSelectedDate(_hours, _selectedTime.minute);
                          },
                          backgroundColor: Colors.transparent,
                          children: [
                            for (int i = 0; i < 24; i++)
                              i < 10
                                  ? Text(
                                      '0$i',
                                      style: FontStyles.kMontserratBoldWhite32,
                                    )
                                  : Text(
                                      '$i',
                                      style: FontStyles.kMontserratBoldWhite32,
                                    ),
                          ],
                        ),
                      ),
                      Spacer(),
                      SizedBox(
                        width: 60.0.w,
                        child: CupertinoPicker(
                          scrollController: FixedExtentScrollController(
                            initialItem: initialTime.minute,
                          ),
                          squeeze: 0.7,
                          itemExtent: 36,
                          diameterRatio: 2,
                          looping: true,
                          onSelectedItemChanged: (i) {
                            _minutes = i;
                            updateSelectedDate(_selectedTime.hour, _minutes);
                          },
                          backgroundColor: Colors.transparent,
                          children: [
                            for (int i = 0; i < 60; i++)
                              i < 10
                                  ? Text(
                                      '0$i',
                                      style: FontStyles.kMontserratBoldWhite32,
                                    )
                                  : Text(
                                      '$i',
                                      style: FontStyles.kMontserratBoldWhite32,
                                    ),
                          ],
                        ),
                      ),
                      Spacer(
                        flex: 3,
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
        IgnorePointer(
          child: Center(
            child: Container(
              height: 65.0.h,
              decoration: BoxDecoration(
                  border: Border(
                    top: BorderSide(color: AppColors.kWhite),
                    bottom: BorderSide(color: AppColors.kWhite),
                  ),
                  color: Colors.transparent),
            ),
          ),
        ),
      ]),
    );
  }

  void updateSelectedDate(int hours, int minutes) {
    _selectedTime = DateTime(
      _selectedTime.year,
      _selectedTime.month,
      _selectedTime.day,
      hours,
      minutes,
    );
  }
}
