import 'package:Provider_template_v1/dictionary/dictionary_classes/create_record_page_language.dart';
import 'package:Provider_template_v1/dictionary/dictionary_classes/pop_up_helper_language.dart';
import 'package:Provider_template_v1/dictionary/dictionary_data/en.dart';
import 'package:Provider_template_v1/dictionary/flutter_dictionary.dart';
import 'package:Provider_template_v1/res/colors.dart';
import 'package:Provider_template_v1/res/fonts.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class DatePicker extends StatelessWidget {
  final DateTime initialDate;
  void Function(DateTime) saveDate;
  DateTime _selectedDate;

  DatePicker({
    @required this.initialDate,
    @required this.saveDate,
  }) {
    _selectedDate = initialDate;
  }

  @override
  Widget build(BuildContext context) {
    CreateRecordPageLanguage datePickerLanguage =
        FlutterDictionary.instance.language?.createRecordPageLanguage ?? en.createRecordPageLanguage;
    PopUpHelperLanguage language = FlutterDictionary.instance.language?.popUpHelperLanguage ?? en.popUpHelperLanguage;

    return SizedBox(
      height: 371.0.h,
      width: 385.0.w,
      child: Stack(
        children: [
          Container(height: 371.0.h, width: 385.0.w, color: AppColors.kBlackTwo),
          Container(
            height: 371.0.h,
            width: 385.0.w,
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [
                  AppColors.kBlack80,
                  Colors.black12,
                  AppColors.kBlack80,
                ],
              ),
            ),
          ),
          Container(
            height: 335.0.h,
            width: 335.0.w,
            padding: const EdgeInsets.all(8.0),
            color: Colors.transparent,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Row(
                  children: [
                    Spacer(),
                    InkWell(
                      onTap: () {
                        Navigator.of(context).pop();
                      },
                      child: Text(
                        language.cancel,
                        style: FontStyles.kMontserratMediumGrey12,
                      ),
                    ),
                    Spacer(
                      flex: 2,
                    ),
                    Text(
                      datePickerLanguage.timeBegin,
                      style: FontStyles.kMontserratSemiBoldWhite14,
                    ),
                    Spacer(
                      flex: 2,
                    ),
                    InkWell(
                      onTap: () {
                        saveDate(_selectedDate);
                        Navigator.of(context).pop();
                      },
                      child: Text(
                        language.save,
                        style: FontStyles.kMontserratMediumBlue12,
                      ),
                    ),
                    Spacer(),
                  ],
                ),
                SizedBox(
                  height: 275.0.h,
                  width: 450.0.w,
                  child: CupertinoTheme(
                    data: CupertinoThemeData(
                      brightness: Brightness.dark,
                      textTheme: CupertinoTextThemeData(dateTimePickerTextStyle: FontStyles.kMontserratBoldWhite22),
                    ),
                    child: CupertinoDatePicker(
                      initialDateTime: initialDate,
                      minimumDate: DateTime.now(),
                      mode: CupertinoDatePickerMode.date,
                      onDateTimeChanged: (newDate) {
                        _selectedDate = newDate;
                      },
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
