import 'package:flutter/cupertino.dart';

class RadioModel {
  bool isSelected;
  final Color leftColor;
  final Color rightColor;

  RadioModel({
    @required this.isSelected,
    @required this.leftColor,
    @required this.rightColor,
  });


}
