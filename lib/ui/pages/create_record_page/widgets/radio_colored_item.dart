import 'package:Provider_template_v1/res/colors.dart';
import 'package:Provider_template_v1/ui/pages/create_record_page/widgets/radio_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class RadioColoredItem extends StatelessWidget {
  RadioModel radioModel;

  RadioColoredItem({@required this.radioModel});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 42.0.sp,
      width: 42.0.sp,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(12.0),
        gradient: LinearGradient(
          colors: [radioModel.leftColor, radioModel.rightColor],
        ),
      ),
      child: radioModel.isSelected ? Icon(
        Icons.check,
        size: 21.0.h,
        color: AppColors.kWhite,
      ): SizedBox(height: 21,),
    );
  }
}
