import 'package:Provider_template_v1/dictionary/dictionary_classes/pop_up_helper_language.dart';
import 'package:Provider_template_v1/dictionary/dictionary_data/en.dart';
import 'package:Provider_template_v1/dictionary/flutter_dictionary.dart';
import 'package:Provider_template_v1/res/colors.dart';
import 'package:Provider_template_v1/res/fonts.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter/material.dart';

class PricePicker extends StatelessWidget {
  void Function(double) savePrice;
  TextEditingController selectedPriceController = TextEditingController();
  final double initialPrice;

  PricePicker({
    @required this.savePrice,
    @required this.initialPrice,
  });

  @override
  Widget build(BuildContext context) {
    PopUpHelperLanguage language = FlutterDictionary.instance.language?.popUpHelperLanguage ?? en.popUpHelperLanguage;
    return Container(
      height: 200.0.h,
      color: AppColors.kWhite,
      padding: const EdgeInsets.all(8.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          TextField(
            controller: selectedPriceController,
            keyboardType: TextInputType.number,
            decoration: InputDecoration(
              labelText: "$initialPrice",
              fillColor: AppColors.kBluishPurple,
              hoverColor: Colors.transparent,
              alignLabelWithHint: true,
//              errorText: isError ? 'Please enter price' : null,
              border: OutlineInputBorder(
                borderSide: BorderSide(width: 0.5),
                borderRadius: BorderRadius.circular(15),
              ),
            ),
          ),
          Divider(),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              InkWell(
                onTap: () {
                  Navigator.of(context).pop();
                },
                child: Text(
                  language.cancel,
                  style: FontStyles.kMontserratMediumGrey12,
                ),
              ),
              InkWell(
                onTap: () {
                  if (!selectedPriceController.text.isEmpty) {
                    savePrice(double.parse(selectedPriceController.value.text));
                  }else {
                  }

                  Navigator.of(context).pop();
                },
                child: Text(
                  language.save,
                  style: FontStyles.kMontserratMediumBlue12,
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}
