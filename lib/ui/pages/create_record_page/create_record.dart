import 'package:Provider_template_v1/data/models/record.dart';
import 'package:Provider_template_v1/dictionary/dictionary_classes/create_record_page_language.dart';
import 'package:Provider_template_v1/dictionary/dictionary_data/en.dart';
import 'package:Provider_template_v1/dictionary/flutter_dictionary.dart';
import 'package:Provider_template_v1/helpers/pop_up_helper.dart';
import 'package:Provider_template_v1/helpers/route_helper.dart';
import 'package:Provider_template_v1/providers/create_record_page_provider.dart';
import 'package:Provider_template_v1/providers/record_provider.dart';
import 'package:Provider_template_v1/res/colors.dart';
import 'package:Provider_template_v1/res/fonts.dart';
import 'package:Provider_template_v1/res/routes.dart';
import 'package:Provider_template_v1/ui/layouts/main_layout.dart';
import 'package:Provider_template_v1/ui/pages/create_record_page/widgets/custom_blue_box.dart';
import 'package:Provider_template_v1/ui/pages/create_record_page/widgets/radio_colored_item.dart';
import 'package:Provider_template_v1/ui/pages/create_record_page/widgets/radio_model.dart';
import 'package:Provider_template_v1/ui/shared/main_app_bar.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class CreateRecordPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final recordData = Provider.of<RecordProvider>(context, listen: false);
    final _createRecordFormKey = GlobalKey<FormState>();
    CreateRecordPageLanguage language =
        FlutterDictionary.instance.language?.createRecordPageLanguage ?? en.createRecordPageLanguage;
    final createRecordData = Provider.of<CreateRecordPageProvider>(context, listen: false);

    return MainLayout(
      child: Form(
        key: _createRecordFormKey,
        child: SingleChildScrollView(
          child: Column(
            children: [
              TextFormField(
                decoration: InputDecoration(
                  alignLabelWithHint: true,
                  labelText: language.name,
                  labelStyle: TextStyle(color: AppColors.kBlack),
                ),
                onSaved: (val) {
                  createRecordData.record.name = val;
                },
                validator: (value) {
                  if (value.isEmpty) {
                    return language.fillFields;
                  }
                  return null;
                },
              ),
              SizedBox(height: 33.0.h),
              Row(
                children: [
                  InkWell(
                    onTap: () {
                      PopUpHelper.instance.showDatePicker(
                          context: context,
                          initialDate: DateTime.now().add(
                            Duration(seconds: 1),
                          ),
                          saveDate: createRecordData.saveDateCome);
                    },
                    child: Consumer<CreateRecordPageProvider>(
                      builder: (ctx, createData, ch) {
                        return CustomBlueBox(
                            centerText: '${DateFormat('M.d').format(createData.record.dateCome)}',
                            descriptionText: language.dateCome);
                      },
                    ),
                  ),
                  SizedBox(width: 32.0.w),
                  InkWell(onTap: () {
                    PopUpHelper.instance.showPricePicker(
                        context: context,
                        initialPrice: createRecordData.record.price ?? 0.0,
                        newPrice: createRecordData.savePrice);
                  }, child: Consumer<CreateRecordPageProvider>(
                    builder: (ctx, createData, ch) {
                      return CustomBlueBox(
                          centerText: '${createData.record.price.round()}', descriptionText: language.price);
                    },
                  )),
                ],
              ),
              SizedBox(height: 33.0.h),
              Row(
                children: [
                  InkWell(
                    onTap: () {
                      PopUpHelper.instance.showTimePicker(
                        context: context,
                        initialDateTime: createRecordData.record.timeBegin,
                        saveDate: createRecordData.saveBeginTime,
                      );
                      print(createRecordData.record.timeBegin);
                    },
                    child: Consumer<CreateRecordPageProvider>(builder: (ctx, createData, ch) {
                      return CustomBlueBox(
                        centerText: '${DateFormat('Hm').format(createData.record.timeBegin)}',
                        descriptionText: language.timeBegin,
                      );
                    }),
                  ),
                  SizedBox(width: 32.0.w),
                  InkWell(
                    onTap: () {
                      PopUpHelper.instance.showTimePicker(
                        context: context,
                        initialDateTime: createRecordData.record.timeBegin.add(Duration(hours: 1)),
                        saveDate: createRecordData.saveEndTime,
                      );
                    },
                    child: Consumer<CreateRecordPageProvider>(builder: (ctx, createData, ch) {
                      return CustomBlueBox(
                          centerText:
                              '${createData.record.timeEnd != null ? DateFormat('Hm').format(createRecordData.record.timeEnd) : ''}',
                          descriptionText: language.timeEnd);
                    }),
                  ),
                ],
              ),
              SizedBox(height: 32.0.h),
              TextFormField(
                onSaved: (val) {
                  createRecordData.record.category = val;
                },
                validator: (value) {
                  if (value.isEmpty) {
                    return language.fillFields;
                  }
                  return null;
                },
                decoration: InputDecoration(
                  alignLabelWithHint: true,
                  labelText: language.service,
                  hintText: language.addServiceHint,
                  labelStyle: TextStyle(color: AppColors.kBlack),
                ),
              ),
              SizedBox(height: 50.0.h),
              TextFormField(
                onSaved: (val) {
                  createRecordData.record.description = val;
                },
                decoration: InputDecoration(
                  labelText: language.description,
                  labelStyle: TextStyle(color: AppColors.kBlack),
                  alignLabelWithHint: true,
                  border: OutlineInputBorder(
                    borderSide: BorderSide(width: 0.5),
                  ),
                  contentPadding: EdgeInsets.only(left: 5.0, top: 15.0),
                ),
                maxLines: 6,
              ),
              SizedBox(height: 32.0.h),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(language.color),
                  SizedBox(height: 4.0.h),
                  SizedBox(
                    height: 58.0.h,
                    width: double.infinity,
                    child: Consumer<CreateRecordPageProvider>(builder: (ctx, createData, ch) {
                      return ListView.builder(
                        itemCount: createRecordData.radioItems.length,
                        itemBuilder: (ctx, index) {
                          return Row(
                            children: [
                              InkWell(
                                splashColor: Colors.transparent,
                                highlightColor: Colors.transparent,
                                onTap: () {
//                                for (RadioModel radio in createRecordData.radioItems) {
//                                  radio.isSelected = false;
//                                }

                                  createData.selectCard(index);
//                                createRecordData.radioItems[index].isSelected = true;

                                  createData.record.colors = [
                                    createData.radioItems[index].leftColor,
                                    createData.radioItems[index].rightColor
                                  ];
                                },
                                child: RadioColoredItem(
                                  radioModel: createRecordData.radioItems[index],
                                ),
                              ),
                              SizedBox(width: 16.0.w)
                            ],
                          );
                        },
                        scrollDirection: Axis.horizontal,
                      );
                    }),
                  ),
                  SizedBox(height: 40.0.h),
                  SizedBox(
                    width: double.infinity,
                    height: 48.0.h,
                    child: RaisedButton(
                      elevation: 5,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(8),
                      ),
                      color: AppColors.kBigBlueButtonColor,
                      textColor: AppColors.kWhite,
                      child: Text(
                        language.create,
                        style: FontStyles.kMontserratWhiteBold18,
                      ),
                      onPressed: () {
                        final form = _createRecordFormKey.currentState;
                        print(form);
                        if (form.validate()) {
                          _createRecordFormKey.currentState.save();

                          recordData.addRecord(createRecordData.record);
                          createRecordData.record = Record(
                              timeBegin: DateTime.now(),
                              timeEnd: null,
                              colors: [],
                              description: '',
                              dateCome: DateTime.now(),
                              price: 0,
                              name: '',
                              category: '');
                          RouteHelper.instance.pushReplacement(context, Routes.kMainPage);
                        } else {
                          PopUpHelper.instance.showErrorPopUp(
                            ctx: context,
                            errorText: language.fillFields,
                          );
                        }
                      },
                    ),
                  ),
                ],
              ),
              SizedBox(height: 32.0.h),
            ],
          ),
        ),
      ),
      appBar: MainAppBar(
          title: language.appBarTitle, previousRoute: RouteHelper.instance.getPreviousPageName().substring(1)),
    );
  }
}
