import 'package:Provider_template_v1/dictionary/dictionary_classes/history_page_language.dart';
import 'package:Provider_template_v1/dictionary/dictionary_data/en.dart';
import 'package:Provider_template_v1/dictionary/flutter_dictionary.dart';
import 'package:Provider_template_v1/helpers/route_helper.dart';
import 'package:Provider_template_v1/providers/record_provider.dart';
import 'package:flutter/material.dart';

import 'package:Provider_template_v1/ui/layouts/main_layout.dart';
import 'package:Provider_template_v1/ui/shared/main_app_bar.dart';
import 'package:provider/provider.dart';

// TODO(Alex): remove unused imports
// TODO(Ihor): done
// ignore: use_key_in_widget_constructors
class HistoryPage extends StatefulWidget {
  @override
  _HistoryPageState createState() => _HistoryPageState();
}

class _HistoryPageState extends State<HistoryPage> {
  TextEditingController _searchTextEditingController;

  DateTime selectedDate = DateTime.now();

  @override
  void initState() {
    super.initState();
    _searchTextEditingController = TextEditingController();
  }

  @override
  Widget build(BuildContext context) {
    final HistoryPageLanguage language =
        FlutterDictionary.instance.language?.historyPageLanguage ?? en.historyPageLanguage;
    return MainLayout(
      child: Center(child: Consumer<RecordProvider>(
        builder: (ctx, recordData, ch) {
          return Column(
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20.0),
                child: TextField(
                  controller: _searchTextEditingController,
                  keyboardType: TextInputType.datetime,
                  textInputAction: TextInputAction.search,
                  decoration: InputDecoration(
                    isDense: true,
                    contentPadding: const EdgeInsets.all(3.0),
                    prefixIcon: Icon(Icons.search),
                    hintText: language.kSearchText,
                    border: OutlineInputBorder(
                      borderSide: BorderSide(
                        width: 5,
                      ),
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                  ),
                ),
              ),
//          recordData.searchDate(selectedDate).length<0?Text(''):ListView(children: [],)
            ],
          );
        },
      )),
      appBar: MainAppBar(
        title: 'History',
        previousRoute: RouteHelper.instance.getPreviousPageName().substring(1),
      ),
    );
  }
}
