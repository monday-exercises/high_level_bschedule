import 'package:Provider_template_v1/dictionary/dictionary_classes/settings_page_language.dart';
import 'package:Provider_template_v1/dictionary/dictionary_data/en.dart';
import 'package:Provider_template_v1/dictionary/flutter_dictionary.dart';
import 'package:Provider_template_v1/helpers/pop_up_helper.dart';
import 'package:Provider_template_v1/helpers/route_helper.dart';
import 'package:Provider_template_v1/res/colors.dart';
import 'package:Provider_template_v1/res/fonts.dart';
import 'package:Provider_template_v1/res/routes.dart';
import 'package:Provider_template_v1/ui/layouts/main_layout.dart';
import 'package:Provider_template_v1/ui/shared/main_app_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class SettingsPage extends StatelessWidget {
  SettingsPageLanguage language = FlutterDictionary.instance.language?.settingsPageLanguage ?? en.settingsPageLanguage;

  @override
  Widget build(BuildContext context) {
    // TODO(Alex): use autoformatting
    // TODO(Alex): never explicitly set null to any parameter
    // TODO(Alex): add coma
    return MainLayout(
      child: Column(
        children: [
          SizedBox(height: 32.0.h),
          InkWell(
            splashColor: Colors.transparent,
            highlightColor: Colors.transparent,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  language.language,
                  style: FontStyles.kEditText,
                ),
                Divider(
                  thickness: 1.0.h,
                ),
              ],
            ),
            onTap: () {
//              Navigator.of(context).pushNamed(Routes.kLanguagesPage);
              RouteHelper.instance.push(context, Routes.kLanguagesPage);
            },
          ),
          SizedBox(height: 24.0.h),
          InkWell(
            splashColor: Colors.transparent,
            highlightColor: Colors.transparent,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  language.aboutUs,
                  style: FontStyles.kEditText,
                ),
                Divider(
                  thickness: 1.0.h,
                ),
              ],
            ),
            onTap: () {
//              Navigator.of(context).pushNamed(Routes.kAboutUsPage);
              RouteHelper.instance.push(context, Routes.kAboutUsPage);
            },
          ),
        ],
      ),
      appBar: MainAppBar(
        title: language.kAppBarTitle,
        previousRoute: Routes.kMainPage.substring(1),
      ),
    );
  }
}
