import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class DialogLayout extends StatelessWidget {
  final Widget child;

  DialogLayout({@required this.child});

  @override
  Widget build(BuildContext context) {
    return Dialog(
      backgroundColor: Colors.transparent,
      elevation: 0,
      child: ClipRRect(borderRadius: BorderRadius.circular(8.0), child: child),
    );
  }
}
