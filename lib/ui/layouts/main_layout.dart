import 'package:Provider_template_v1/helpers/pop_up_helper.dart';
import 'package:Provider_template_v1/helpers/route_helper.dart';
import 'package:Provider_template_v1/res/colors.dart';
import 'package:Provider_template_v1/res/routes.dart';
import 'package:Provider_template_v1/ui/layouts/focus_layout.dart';
import 'package:Provider_template_v1/ui/shared/custom_bottom_navigation_bar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

// TODO(Alex): remove unused imports
// TODO(Ihor): done
class MainLayout extends StatelessWidget {
  final Widget child;
  final Widget appBar;
  bool isFloatingActionBtn;

  MainLayout({
    @required this.child,
    @required this.appBar,
    this.isFloatingActionBtn = false,
  });

  @override
  Widget build(BuildContext context) {
    return FocusLayout(
      child: Scaffold(
        floatingActionButton: _getFloatingActionButton(context),
        resizeToAvoidBottomPadding: false,
        resizeToAvoidBottomInset: true,
        appBar: appBar,
        backgroundColor: Colors.white,
        bottomNavigationBar: CustomNavigationBottomBar(),
        body: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20.0),
          child: child,
        ),
      ),
    );
  }

  Widget _getFloatingActionButton(context) {
    if (isFloatingActionBtn) {
      return FloatingActionButton(
        onPressed: () {
//          Navigator.of(context).pushNamed(Routes.kCreateRecordPage);
          RouteHelper.instance.push(context, Routes.kCreateRecordPage);
//        PopUpHelper.instance.showOnBoardingPopUp(context);
        },
        child: Center(
          child: Icon(
            CupertinoIcons.add,
            size: 64.h,
            color: AppColors.kWhite,
          ),
        ),
      );
    }
    return null;
  }
}
