import 'package:flutter/material.dart';

class FocusLayout extends StatelessWidget {
  final Widget child;

  FocusLayout({@required this.child});

  @override
  Widget build(BuildContext context) {
    return Material(
      child: InkWell(
        onTap: () {
          FocusScope.of(context).requestFocus(FocusNode());
        },
        child: child,
      ),
    );
  }
}
