import 'package:Provider_template_v1/helpers/route_helper.dart';
import 'package:Provider_template_v1/res/colors.dart';
import 'package:Provider_template_v1/res/routes.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:google_fonts/google_fonts.dart';

class MainAppBar extends StatelessWidget implements PreferredSizeWidget {
  final String title;
  final String previousRoute;

  // TODO(Alex): prefer named parameters
  // TODO(Ihor): done
  MainAppBar({
    @required this.title,
    @required this.previousRoute,
  });

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
        height: 54.h,
        color: Colors.white,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            SizedBox(width: 20.0.w),
            InkWell(
              // TODO(Alex): make this constant
              // TODO(Ihor): done
              /// padding: const EdgeInsets.only(top: 8.0, left: 8.0, bottom: 8.0, right: 5.0),
              child: Row(
                children: [
                  Icon(
                    Icons.arrow_back_ios,
                    size: 18.sp,
                    color: AppColors.kGrey,
                  ),
                  SizedBox(
                    height: 18.h,
                    width: 52.w,
                    child: Text(
                      // TODO(Alex): move this to dictionary
                      previousRoute,
                      style: GoogleFonts.montserrat(
                        color: AppColors.kGrey,
                        fontSize: 12.sp,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ],
              ),
              onTap: () {
                RouteHelper.instance.pushReplacement(context, RouteHelper.instance.getPreviousPageName());
              },
            ),
            const Spacer(),
            Text(
              title,
              style: GoogleFonts.montserrat(
                color: Colors.black,
                fontSize: 24.sp,
                fontWeight: FontWeight.bold,
              ),
            ),
            const Spacer(flex: 4),
          ],
        ),
      ),
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(54.0.h);
}
