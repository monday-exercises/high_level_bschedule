import 'package:Provider_template_v1/helpers/route_helper.dart';
import 'package:Provider_template_v1/providers/bottom_navigation_provider.dart';
import 'package:Provider_template_v1/res/colors.dart';
import 'package:Provider_template_v1/res/routes.dart';

import 'package:Provider_template_v1/utils/icons/my_flutter_app_icons.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class CustomNavigationBottomBar extends StatefulWidget {
  @override
  _CustomNavigationBottomBarState createState() => _CustomNavigationBottomBarState();
}

class _CustomNavigationBottomBarState extends State<CustomNavigationBottomBar> {
  // TODO(Alex): uncomment TODO's
//  // TODO(Alex): remove unused variables and functions
//  int _selectedPageIndex = 0;
//
//  // TODO(Alex): add coma
//  final List<Widget> _pages = [
//    HistoryPage(),
//    MainPage(),
//    SettingsPage(),
//  ];
//
//  void _selectPage(int index) {
//    setState(() {
//      _selectedPageIndex = index;
//    });
//  }

  @override
  Widget build(BuildContext context) {
    final data = Provider.of<BottomNavigationProvider>(context);
    return Container(
      color: AppColors.kBottomNavigationColor,
      height: 60.0.h,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          InkWell(
            splashColor: Colors.transparent,
            highlightColor: Colors.transparent,
            onTap: () {
              data.buttonIndex = ButtonIndex.time;
//              Navigator.of(context).pushReplacementNamed(Routes.kHistoryPage);
              RouteHelper.instance.pushReplacement(context, Routes.kHistoryPage);

            },
            child: Icon(
              AppIcons.time_icon,
              color:
              data.buttonIndex == ButtonIndex.time ? AppColors.kBottomNavigationButtonColor : AppColors.kGrey,
            size: 26.0.w,),
          ),
          InkWell(
            splashColor: Colors.transparent,
            highlightColor: Colors.transparent,
            child: Icon(
              AppIcons.home_icon,
              color:
              data.buttonIndex == ButtonIndex.home ? AppColors.kBottomNavigationButtonColor : AppColors.kGrey,
              size: 30.0.w,
            ),
            onTap: () {
              data.buttonIndex = ButtonIndex.home;
//              Navigator.of(context).pushReplacementNamed(Routes.kMainPage);
              RouteHelper.instance.pushReplacement(context, Routes.kMainPage);

            },
          ),
          InkWell(
            splashColor: Colors.transparent,
            highlightColor: Colors.transparent,
            child: Icon(
              AppIcons.settings_icon,
              color: data.buttonIndex == ButtonIndex.settings
                  ? AppColors.kBottomNavigationButtonColor
                  : AppColors.kGrey,
              size: 26.0.w,
            ),
            onTap: () {
              data.buttonIndex = ButtonIndex.settings;
//              Navigator.of(context).pushReplacementNamed(Routes.kSettingsPage);
              RouteHelper.instance.pushReplacement(context, Routes.kSettingsPage);

            },
          ),
        ],
      ),
    );
  }
}
