import 'package:Provider_template_v1/helpers/pop_up_helper.dart';
import 'package:Provider_template_v1/providers/calendar_provider.dart';
import 'package:Provider_template_v1/providers/record_provider.dart';
import 'package:Provider_template_v1/res/colors.dart';
import 'package:Provider_template_v1/res/fonts.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class CustomAppBar extends StatelessWidget implements PreferredSizeWidget {
  @override
  Widget build(BuildContext context) {
    final recordProviderData = Provider.of<RecordProvider>(context);

    return SafeArea(
      child: Consumer<CalendarProvider>(
        builder: (ctx, calendarData, ch) {
          return Container(
            height: 54.h,
            color: Colors.white,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Spacer(flex: 3),
                InkWell(
                  child: Icon(
                    Icons.arrow_back_ios,
                    color: AppColors.kGrey,
                    size: 25,
                  ),
                  onTap: () {
                    calendarData.decrementDate();
                    print(recordProviderData.selectedDate);
                  },
                ),
                const Spacer(),
                Text(
                  '${DateFormat('EEEE').format(calendarData.selectedDate)}',
                  style: GoogleFonts.montserrat(
                    color: Colors.black,
                    fontSize: 24.sp,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      '${DateFormat.d().format(calendarData.selectedDate)}',
                      style: FontStyles.kMontserratSemiBoldGrey14,
                    ),
                    Spacer()
                  ],
                ),
                const Spacer(),
                InkWell(
                  child: Icon(
                    Icons.arrow_forward_ios,
                    color: AppColors.kGrey,
                    size: 25,
                  ),
                  onTap: () {
                    calendarData.incrementDate();
                  },
                ),
                const Spacer(flex: 2),
                ch,
                const Spacer()
              ],
            ),
          );
        },
        child: InkWell(
          child: Transform.rotate(
            angle: 110,
            child: Icon(
              Icons.info_outline,
              color: AppColors.kGrey,
              size: 30,
            ),
          ),
          onTap: () {
            PopUpHelper.instance.showOnBoardingPopUp(context);
          },
        ),
      ),
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(54.0.h);
}
