import 'package:Provider_template_v1/dictionary/dictionary_classes/onboarding_page_language.dart';
import 'package:Provider_template_v1/dictionary/dictionary_data/en.dart';
import 'package:Provider_template_v1/dictionary/flutter_dictionary.dart';
import 'package:Provider_template_v1/providers/on_boarding_pointer_provider.dart';
import 'package:Provider_template_v1/res/colors.dart';
import 'package:Provider_template_v1/res/fonts.dart';
import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:provider/provider.dart';

class OnBoarding extends StatelessWidget {
  final OnBoardingPageLanguage onBoardingPopUpLanguage =
      FlutterDictionary.instance.language?.onBoardingPageLanguage ?? en.onBoardingPageLanguage;
  Map onBoardingItems;

  OnBoarding() {
    onBoardingItems = {
      '${onBoardingPopUpLanguage.createRecord}': 'assets/images/on_boarding_create.png',
      '${onBoardingPopUpLanguage.fillRecord}': 'assets/images/on_boarding_fill.png',
      '${onBoardingPopUpLanguage.manageRecord}': 'assets/images/on_boarding_manage.png',
    };
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 550.h,
      width: 342.w,
      padding: const EdgeInsets.all(8.0),
      child: Consumer<OnBoardingPointerProvider>(
        builder: (ctx, pointer, ch) {
          return Column(
            children: [
              Text(
                onBoardingItems.keys.toList()[pointer.currentIndex],
                style: FontStyles.kMontserratSemiBoldWhite14,
                maxLines: 2,
              ),
              SizedBox(height: 16.0.h),
              CarouselSlider.builder(
                options: CarouselOptions(
                    height: 380.h,
                    viewportFraction: 1,
                    onPageChanged: (index, _) {
                      pointer.currentIndex = index;
                    }),
                itemCount: onBoardingItems.length,
                itemBuilder: (ctx, index) {
                  return Image.asset(
                    onBoardingItems.values.toList()[index],
                    width: 313.w,
                    height: 362.h,
                  );
                },
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  SizedBox(
                    width: 60.0.w,
                    height: 50.0.h,
                    child: ListView.builder(
                        physics: NeverScrollableScrollPhysics(),
                        scrollDirection: Axis.horizontal,
                        itemCount: onBoardingItems.length,
                        itemBuilder: (_, index) {
                          return Row(
                            children: [
                              Container(
                                height: 8.sp,
                                width: 8.sp,
                                decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  color: index == pointer.currentIndex ? AppColors.kWhite : AppColors.kGrey,
                                ),
                              ),
                              SizedBox(width: 16.0.w),
                            ],
                          );
                        }),
                  ),
                  SizedBox(width: 95.0.w),
                  InkWell(
                    onTap: Navigator.of(context).pop,
                    child: Text(
                      onBoardingPopUpLanguage.miss,
                      style: FontStyles.kMontserratMediumWhite12,
                    ),
                  )
                ],
              )
            ],
          );
        },
      ),
    );
  }
}
