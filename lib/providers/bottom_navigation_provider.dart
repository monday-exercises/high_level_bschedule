import 'package:flutter/material.dart';

enum ButtonIndex { time, home, settings }

class BottomNavigationProvider with ChangeNotifier {
  ButtonIndex _buttonIndex;

  ButtonIndex get buttonIndex => _buttonIndex;

  set buttonIndex(ButtonIndex value) {
    _buttonIndex = value;
    notifyListeners();
  }
}
