import 'package:Provider_template_v1/data/models/record.dart';
import 'package:Provider_template_v1/res/colors.dart';
import 'package:Provider_template_v1/ui/pages/create_record_page/widgets/radio_model.dart';
import 'package:flutter/cupertino.dart';

class CreateRecordPageProvider with ChangeNotifier {
  Record record = Record(
      timeBegin: DateTime.now(),
      timeEnd: null,
      colors: [],
      description: '',
      dateCome: DateTime.now(),
      price: 0,
      name: '',
      category: '');

  final List<RadioModel> _radioItems = [
    RadioModel(
      isSelected: false,
      leftColor: AppColors.kDodgerBlue,
      rightColor: AppColors.kDodgerBlue,
    ),
    RadioModel(
      isSelected: false,
      leftColor: AppColors.kBrightSkyBlue,
      rightColor: AppColors.kBrightBlue,
    ),
    RadioModel(
      isSelected: false,
      leftColor: AppColors.kSlate,
      rightColor: AppColors.kDark,
    ),
    RadioModel(
      isSelected: false,
      leftColor: AppColors.kHotPink,
      rightColor: AppColors.kSalmon,
    ),
    RadioModel(
      isSelected: false,
      leftColor: AppColors.kNeonPurple,
      rightColor: AppColors.kBluishPurple,
    ),
    RadioModel(
      isSelected: false,
      leftColor: AppColors.kTealish,
      rightColor: AppColors.kBlueberry,
    ),
  ];

  List<RadioModel> get radioItems => _radioItems;


 void selectCard(int index) {
    for (RadioModel radio in radioItems) {
      radio.isSelected = false;
    }
    radioItems[index].isSelected = true;
    notifyListeners();
  }


  void saveBeginTime(DateTime time) {
    record.timeBegin = time;
    notifyListeners();
  }

  void saveEndTime(DateTime time) {
    record.timeEnd = time;
    notifyListeners();
  }

  void savePrice(double price) {
    record.price = price;
    notifyListeners();
  }

  void saveDateCome(DateTime dateCome) {
    record.dateCome = dateCome;
    notifyListeners();
  }
}
