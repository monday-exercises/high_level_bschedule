import 'package:flutter/material.dart';

class CalendarProvider with ChangeNotifier {
  DateTime selectedDate = DateTime.now();


  void incrementDate() {
   selectedDate =  selectedDate.add(
      Duration(days: 1)
    );
    notifyListeners();
  }

  void decrementDate() {
   selectedDate = selectedDate.subtract(
      Duration(days: 1),
    );
    notifyListeners();
  }
}
